const express = require("express");
const router = express.Router();
const ObjectId = require('mongoose').Types.ObjectID;

const { CommentsModel } = require('../models/commentsModel');

router.get('/all', (req, res) => {
    CommentsModel.find((err, docs) => {
        if (!err) res.send(docs)
        else console.log("Error to get data : " + err )
    })
});

router.post('/', (req, res) => {
    const newComment = new CommentsModel({
        author: req.body.author,
        message: req.body.message
    });

    newComment.save((err,docs) => {
        if (!err) res.send(docs);
        else console.log("Error response: " + err ); 
    })
})

router.get('/:id',(req, res) => {
    CommentsModel.findById(req.params.id, (err,docs) => {
        if (!err) res.send(docs)
        else return res.status(400).send("L'utilsateur est introuvable");
    })
})

module.exports = router;