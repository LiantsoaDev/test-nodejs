const express = require('express');
const router = express.Router();

const { CarsModel } = require('../models/carsModel');

router.get('/', (req, res) => {
    CarsModel.find((err, docs) => {
       if (!err) res.send(docs)
       else console.log("Error to get data : " + err )
    })
} )

module.exports = router