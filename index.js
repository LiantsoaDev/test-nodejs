const express = require('express');
const app = express();
const bodyParser = require('body-parser');
require('./models/dbConfig');


const carsRoutes = require('./routes/carsController');
//const commentsRoutes = require('./routes/commentsController');

app.use(express.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(express.json());

app.use('/cars', carsRoutes);

//app.use('/comments', commentsRoutes);

app.listen(5500, () => console.log('Server started : 5500'));