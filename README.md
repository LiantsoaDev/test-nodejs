## Pré-requis et dépendance

- [node js](https://nodejs.org/fr/download/).
- [MongoDB Compass](https://www.mongodb.com/try/download/compass)

## Importer la base de donnée dans MongoDB Compass

importer les fichiers `cars.json` et `comments.json` dans votre gestionnaire de base de donnée par exemple ``MongoDB Compass```

## Installer les dépendances

Aller dans le répertoire du projet puis executez la commande suivante pour charger le dépendance : 

```
npm install
```

## Démarrer MongoDB Compass et node js 

Après avoir démarré `MongoDB` et `MongoDB Compass`, aller dans le répertoire du projet puis executez la commande suivante dans votre `Terminal` ou `Shell`:

```
npm start
```

## Node JS démarre généralement sur le port 

```
http://localhost:5500
```

## API cars : 

```
http://localhost:5500/cars
```
