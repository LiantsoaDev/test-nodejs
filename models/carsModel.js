const mongoose = require("mongoose");
const CarsModel  = mongoose.model(
    "node-api",
    {
        marque: {
            type: String,
            required: true
        },
        modele: {
            type: String,
            required: true
        }, 
        annee: {
            type: String,
            required: true
        },
        nb_places: {
            type: String,
            required: true
        },
        image :{
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    },
    "cars"
);

module.exports = { CarsModel }