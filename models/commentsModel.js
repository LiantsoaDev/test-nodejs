const mongoose = require("mongoose");
const CommentsModel = mongoose.model(
    "node-api",
    {
        author: {
            type: String,
            required: true
        },
        message: {
            type:String,
            required: true
        },
        date: {
             type: Date,
             default: Date.now
        }
    },
    "comments"
);

module.exports = { CommentsModel };